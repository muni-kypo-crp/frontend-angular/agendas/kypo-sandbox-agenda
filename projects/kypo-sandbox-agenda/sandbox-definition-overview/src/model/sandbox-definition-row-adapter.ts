import { SandboxDefinition } from '@muni-kypo-crp/sandbox-model';

export class SandboxDefinitionRowAdapter extends SandboxDefinition {
  createdByName: string;
  titleWithRevision: string;
}
