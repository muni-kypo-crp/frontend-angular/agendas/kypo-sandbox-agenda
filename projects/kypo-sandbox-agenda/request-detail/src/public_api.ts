/*
 * Public API Surface of entry point kypo-sandbox-agenda/request-detail
 */

export * from './components/allocation/allocation-request-detail-components.module';
export * from './components/cleanup/cleanup-request-detail-components.module';
export * from './components/allocation/allocation-request-detail.component';
export * from './components/cleanup/cleanup-request-detail.component';

export * from './services/state/request-stages.service';
export * from './services/state/detail/stage-detail.service';
