18.0.0 Update to Angular 18.
16.2.7 Fix cleanup failure message.
16.2.6 Revert pool locking changes.
16.2.5 Fix and improve allocate sandboxes dialog.
16.2.4 Fix incorrect training api provisioning.
16.2.3 Revert package-lock version.
16.2.2 Revert gitlab-ci change.
16.2.1 Implement new endpoints and improve pool locking.
16.2.0 Changes resources tab to images and remove resources overview from it.
16.1.3 Set default sorting direction and column for pool-detail and pool-overview.
16.1.2 Update pagination to save size by component id.
16.1.1 Update all components to work without a deprecated Sentinel directive.
16.1.0 Enable pool size change for existing pool with allocations.
16.0.4 Adjust styling of pool overview components.
16.0.3 Fix and unify acquisition of sandbox ids and pool deletion.
16.0.2 Add an extended graphical view for pool overview, enable sending notifications upon sandbox building.
16.0.1 Enable a force deletion of pools with allocations.
16.0.0 Update to Angular 16, add keycloak, introduce pool and sandbox comments, enable pool edition.
15.2.0 Update sandbox definition placeholder url to http format.
15.1.3 Adjust and specify sandbox allocation label and tooltip.
15.1.2 Update sandbox api to accept paginated allocation units.
15.1.1 Fix sandbox definition faulty buttons.
15.1.0 Adjust pool view to communicate with optimized sandbox service.
15.0.1 Update agenda cards styling.
15.0.0 Update to Angular 15.
14.4.7 Fix the retrieval of poolId upon a single sandbox allocation.
14.4.6 Add sorting options for pool, sandbox definition and resources tables.
14.4.5 Add option to confirm sandbox definition creation by pressing enter and added whitespace trimming. Added ports and networks to resources. Added revision to pool overview. Enhanced sandbox allocation confirmation dialog. Disabled lock option when allocation fails.
14.4.4 Update topology view with new legend
14.4.3 Fix sandboxes sorting in pool detail table.
14.4.2 Bump topology version to address fix of collapsed subnet size number.
14.4.1 Resolve problems with guacamole. Fix restrictions for the allocation dialogue and fix force delete for sandbox under allocation.
14.4.0 Added dynamic allocation of sandboxes through the dialog window with slider.
14.3.1 Add option to sort the pool detail table by Create By, Name and Created.
14.3.0 Replace sandbox id with sandbox uuid.
14.2.1 Change delete allocation units buttons to one expandable button with options to delete all, unlocked and failed.
14.2.0 Refactor resources page. Add information about GUI access and version to image. Rearrange information in image column and detail. Add checkbox to filter images with GUI access. Add Header to Images table.
14.1.3 Address changes in delete failed, unlocked and all allocation units.
14.1.2 Fix polling problem caused by pagination setting.
14.1.1 Fix problem with delete action of sandbox instance at pool detail table.
14.1.0 Optimize calls, polling and move spice console loading to topology.
14.0.1 Rename from kypo2 to kypo.
14.0.0 Update to Angular 14.
13.2.6 Bump topology graph to resolve problems with console selection.
13.2.5 Improve images table.
13.2.4 Show additional actions in tables.
13.2.3 Bump version of topology graph
13.2.2 Remove pool size max limitation
13.2.1 Fix table reload on pagination change
13.2.0 Change openstack allocation stage to terraform and adjust stage information. Fix retry stage for pool overview. Add redirect to stage detail upon selecting it in pipeline.
13.1.0 Add display topology option for sandbox definitions in sandbox definition overview
13.0.2 Fix pool creation pagination size for sandbox definition select
13.0.1 Fix peer dependencies of package
13.0.0 Update to Angular 13, CI/CD optimization, retry of allocation pipeline on fail supported, revision and id of SD added to pool creation
12.0.10 Fix pagination
12.0.9 Add support for backend sort
12.0.8 Add sandbox definition name to pool overview
12.0.7 Preload spice in topology. Add created by field to sandbox definition.
12.0.6 Redesign detail page for pools
12.0.5 New version of the topology graph package - added missing configuration for Apache Guacamole.
12.0.4 Add build of example app to CI. Rename endpoint from kypo to kypo. Save user preferred pagination. Change titles for SSH buttons. Add aliases to tsconfig.
12.0.3 Update gitlab CI
